#! /usr/bin/env python3

import filecmp
import shlex
import subprocess

PROGRAM_NAME = "../dka2mka"

RED='\033[1;31m'
GREEN='\033[1;32m'
NC='\033[0m'

def getTests():
    return [
        ["./ref-in/test.in", "./test-out/test00.out", "./ref-out/test00.out", 0, "{} 2>&1 > {}"],
        ["./ref-in/test.in", "./test-out/test00.out", "./ref-out/test00.out", 0, "< {} 2>&1 > {}"],
        ["./ref-in/test.in", "./test-out/test00a.out", "./ref-out/test00.out", 1, "-t -t {} 2>&1 > {}"],
        ["./ref-in/test.in", "./test-out/test00b.out", "./ref-out/test00.out", 1, "-i -i {} 2>&1 > {}"],
        ["./ref-in/test.in", "./test-out/test00c.out", "./ref-out/test00.out", 1, "-k {} 2>&1 > {}"],
        ["./ref-in/test.in", "./test-out/test.out", "./ref-out/test.out", 0, "-t {} 2>&1 > {}"],
        ["./ref-in/test01.in", "./test-out/test01.out", "./ref-out/test01.out", 0, "-t {} 2>&1 > {}"],
        ["./ref-in/test01.in", "./test-out/test01min.out", "./ref-out/test01min.out", 0, "-i {} 2>&1 > {}"],
        ["./ref-in/test01.in", "./test-out/test01min_print.out", "./ref-out/test01min_print.out", 0, "-i -t {} 2>&1 > {}"],
        ["./ref-in/test01.in", "./test-out/test01print_min.out", "./ref-out/test01print_min.out", 0, "-t -i {} 2>&1 > {}"],
        ["./ref-in/test01a.in", "./test-out/test01a.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01b.in", "./test-out/test01b.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01c.in", "./test-out/test01c.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01d.in", "./test-out/test01d.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01e.in", "./test-out/test01e.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01f.in", "./test-out/test01f.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01g.in", "./test-out/test01g.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01h.in", "./test-out/test01h.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01i.in", "./test-out/test01i.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01j.in", "./test-out/test01j.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01k.in", "./test-out/test01k.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01l.in", "./test-out/test01l.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01m.in", "./test-out/test01m.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01n.in", "./test-out/test01n.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01o.in", "./test-out/test01o.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01p.in", "./test-out/test01p.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01q.in", "./test-out/test01q.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test01r.in", "./test-out/test01r.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test02.in", "./test-out/test02.out", "./ref-out/test02.out", 0, "-i {} 2>&1 > {}"],
        ["./ref-in/test03.in", "./test-out/test03.out", "./ref-out/test00.out", 1, "-t {} 2>&1 > {}"],
        ["./ref-in/test04.in", "./test-out/test04.out", "./ref-out/test04.out", 0, "-i {} 2>&1 > {}"],
        ["./ref-in/test05.in", "./test-out/test05.out", "./ref-out/test05.out", 0, "-i {} 2>&1 > {}"],
        ["./ref-in/test06.in", "./test-out/test06.out", "./ref-out/test06.out", 0, "-i {} 2>&1 > {}"],
        ["./ref-in/test07.in", "./test-out/test07.out", "./ref-out/test07.out", 0, "-i {} 2>&1 > {}"],
        ["./ref-in/test08.in", "./test-out/test08a.out", "./ref-out/test08a.out", 0, "-t {} 2>&1 > {}"],
        ["./ref-in/test08.in", "./test-out/test08b.out", "./ref-out/test08b.out", 0, "-i {} 2>&1 > {}"]
    ]

if __name__ == '__main__':
    
    for test in getTests():
        command = PROGRAM_NAME + " " + test[4].format(test[0], test[1])
        process = subprocess.run(command, shell=True, stdout=subprocess.PIPE)
        if process.returncode == test[3]:
            if test[3] == 0:
                diff = filecmp.cmp(test[1], test[2])
                if diff:
                    print(GREEN + "OK\t" + test[4].format(test[0], test[1]) + NC)
                else:
                    print(RED + "ERROR\t" + test[4].format(test[0], test[1]) + NC)
            else:
                print(GREEN + "OK\t" + test[4].format(test[0], test[1]) + NC)
        else:
            print(RED + "ERROR\t" + test[4].format(test[0], test[1]) + NC)
        
        


