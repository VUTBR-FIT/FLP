import Data.List
import Data.Char as Char
import System.Console.GetOpt
import System.Environment
import System.IO
import qualified Data.Set as Set

type State = Int
type Alpha = Char
type ClassIndex = Int
type Class = [State]
type ClassMapping = [(State, [ClassIndex])]
data Rule = Rule {
    from  :: State,
    alpha :: Alpha,
    to    :: State
}
instance Show Rule where
    show (Rule start alpha end) = intercalate  "," [show start, [alpha], show end]

data FA = FA { 
    states   :: [State],
    alphabet :: Set.Set Alpha,
    rules    :: [Rule],
    start    :: State,
    finals   :: [State]
}
instance Show FA where
    show (FA states _ rules start finals) = (intercalate "\n" $ [intercalate "," (map show states), show start, intercalate "," (map show finals)] ++ [show x | x <- rules]) ++ "\n"
        
main = do
    argv <- getArgs
    let (enableI, enableT, fileName, order) = parseParams argv
    if fileName == ""
        then do
            input <- getContents
            doProgram (parseFA input) enableI enableT order
        else do
            handle <- openFile fileName ReadMode
            input <- hGetContents handle
            doProgram (parseFA input) enableI enableT order
            hClose handle 
    
    return 0

-- Parsuje vstupni parametry
-- Vstup: Zadane prepinace
-- Vystup: (I, T, FILE_NAME, ORDER)
--          I     - je True pokud byl zadan prepinac -i
--          T     - je True pokud byl zadan prepinac -t
--          T     - Obsahuje nazev stupniho souboru, podkud byl zadan
--          ORDER - je True pokud prepinac -i by zadan pred prepinacem -t
parseParams :: [String] -> (Bool, Bool, [Char], Bool)
parseParams [] = (False, False, [], False)
parseParams [a] = (a == "-i", a == "-t", if a /= "-i" && a /= "-t" then a else "", a == "-i")
parseParams (a:b:[]) = if (a == "-i" || a == "-t") && a /= b
    then (a == "-i" || b == "-i", a == "-t" || b == "-t", if b /= "-i" && b /= "-t" then b else "", a == "-i")
    else error "Neplatny argument nebo neplatna kombinace argumentu"
parseParams (a:b:c:[]) = if (a == "-i" || a == "-t") && (b == "-i" || b == "-t") && a /= b
    then (a == "-i" || b == "-i", a == "-t" || b == "-t", c, a == "-i")
    else error "Neplatny argument nebo neplatna kombinace argumentu"
parseParams (a:b:c:d:_) = error "Neplatny pocet parametru"

-- Ze vstupniho souboru nacte hodnotu, za ktera nasleduje carka nebo konec radku
-- Vstup: Obsah vstupniho souboru
-- Vystup: (VALUE, RESIDUE, END)
--          VALUE   - Obsahuje nactenou hodnotu
--          RESIDUE - Obsahuje nezpracovany zbytek souboru
--          END     - Typ ukonceni hodnot 0 - carka, 1 - konec radku
getNextValue :: [Char] -> ([Char], [Char], Int)
getNextValue (x:xs) 
    | x == ','  = ([], xs, 0)
    | x == '\n' = ([], xs, 1)
getNextValue (x:end:xs) 
    | end == '\n' = ([x], xs, 1)
    | end == ','  = ([x], xs, 0)
getNextValue (x:xs) = if (ord x >= ord 'a' && ord x <= ord 'z') || (ord x >= ord '0' && ord x <= ord '9') 
    then ([x] ++ part, residue, endLine) 
    else error "Nepovolena hodnota"
    where (part, residue, endLine) = getNextValue xs

-- Overi jestli vstupni retezec je validni stav    
validateState :: [Char] -> State
validateState input = case reads input :: [(Int, String)] of
        [(x, "")]   -> x
        _           -> error "Nazev stavu musi byt tvoreny cisly"

-- Overi jestli vstupni retezec je validni stav abecedy
validateAlpha :: [Char] -> Alpha
validateAlpha input = if length input /= 1 || (isAlpha $ head input) == False
        then error "Nepolatna delka vstupni abecedy"
        else char
    where char = head input

-- Vygeneruje pravidla
-- Vstup: CLASSES MAPPING ALPHABET CLASSINDEX
--      CLASSES    - Tridy stavu
--      MAPPING    - Mapovani stavu na index tridy podle znaku v abecede
--      ALPHABET   - Vstupni abeceda automatu
--      CLASSINDEX - Aktualni index tridy
-- Vystup: Vygenerovana mnozina pravidel
generateRules :: [Class] -> ClassMapping -> [Alpha] -> ClassIndex -> [Rule]
generateRules [] _ _ _ = [] 
generateRules (x:xs) mapping alphabet index = (generate index alphabet (getMapping (head x) mapping)) ++ (generateRules xs mapping alphabet (index+1)) 
    where
        -- Generuji pravidla pro zadany stav a na zaaklade mapovani
        generate state (x:xs) (y:ys) = (if y == -1 then [] else [Rule state x (y+1)]) ++ generate state xs ys
        generate state [] _ = []  

parseFA :: [Char] -> FA
parseFA input = FA tStates tAlphabet tRules tStart tFinals 
    where
        tStart              = validateStart tStart1 tStates
        tRules              = parseRules residue3 tStates
        tAlphabet           = Set.fromList $ getAlphabetFromRules tRules
        (tStates, residue)  = parseStates input
        (tStart1, residue2) = parseStates residue
        (tFinals, residue3) = parseStates residue2
        -- Precte radek ze vstupu a prevede ho na pole stavu, ktere validuje
        -- Vstup: Obsah vstupniho souboru
        -- Vystup: Pole nactenych stavu
        parseStates input = if end == 0
            then ([state] ++ tStates, tResidue)   
            else ([state], residue)
            where
                state                   = validateState tState      
                (tStates, tResidue)     = parseStates residue
                (tState, residue, end)  = getNextValue input
        -- Overi ze se jedna pouze o jeden stav z mnoziny stavu
        -- Vstup: INPUT STATES
        --      INPUT  - Pole se startovacim stavem
        --      STATES - Pole se stavy automatu
        -- Vystup: Statovaci stav
        validateStart input states = if length input /= 1 || (head input) `notElem` states
                then error "Neplatny stav nebo neplatny pocet startovacich stavu"
                else head input 
        -- Precte zbytek obsahu souboru a vytvori z nich validni pravidla
        -- Vstup: obsah vstupniho souboru
        -- Vystup: Pole pravidel
        parseRules input states = if end1 == 0 || length residue1 >= 1
            then if end1 == 0 && end2 == 0 && start `elem` states && end `elem` states
                then [Rule start alpha end] ++ parseRules residue states
                else error "Nevalidni pravidlo"
            else []
            where
                alpha                    = validateAlpha tAlpha
                start                    = validateState tStart
                end                      = validateState tEnd
                (tStart, residue1, end1) = getNextValue input
                (tAlpha, residue2, end2) = getNextValue residue1
                (tEnd, residue, _)    = getNextValue residue2

-- Z predanych pravidel vytvori vstupni abecedu
-- Vstup: Pole pravidel
-- Vystup: Vstupni abeceda
getAlphabetFromRules :: [Rule] -> [Alpha]
getAlphabetFromRules [] = []
getAlphabetFromRules (x:xs) = (if (alpha x) `elem` other then [] else [alpha x]) ++ other
    where other = getAlphabetFromRules xs

-- Najde index tridy, ve ktere se nachazi hledany stav
-- Vstup: FOUND CALSSES INDEX
--      FOUND   - Hledany stav
--      CLASSES - Pole trid
--      INDEX   - Pocatecni index
-- Vystup: Index tridy (index = -1 pokud stav nebyl nalzen)
findClassIndex :: State -> [Class] -> ClassIndex -> ClassIndex
findClassIndex found (x:xs) index = if length (filter (\y -> y == found) x) == 1 then index else findClassIndex found xs (index + 1)
findClassIndex _ [] _ = -1

-- Provede odpovidajici operace podle zadanych prepinacu a jejich poradi
-- Vstup: FA I T ORDER
--      FA    - Zpracovany vstupni automat
--      I     - True pokud byl zadany prepinac -i
--      T     - True pokud byl zadany prepinac -t
--      ORDER - True pokud byl prvni zadan prepinac -i
-- Vystup: Vysledek vypsany na STDOUT
doProgram :: FA -> Bool -> Bool-> Bool -> IO ()
doProgram _ False False _ = putStr ""
doProgram fa enableI enableT order = if enableI == True && enableT == True
    then if order
            then do
                putStrLn $ show $ minimalization fa
                putStrLn $ show fa
            else do 
                putStrLn $ show fa
                putStrLn $ show $ minimalization fa
    else if enableI
        then putStrLn $ show $ minimalization fa
        else putStrLn $ show fa
-- Provede minimalizaci vstupniho automatu
-- Vstup: Deterministicky konecny automat
-- Vystup: Minimalizovany deterministky konecny automat
minimalization :: FA -> FA
minimalization fa = FA tStates tAlphabet tRules tStart tFinals
    where
        tStart              = findClassIndex (start fa) classes 1
        (classes, temp)     = doMinimalization (rules fa) (Set.toList $ alphabet fa) ([finals fa] ++ [removeFromArray (states fa) (finals fa)])
        tRules              = generateRules classes temp (Set.toList tAlphabet) 1
        tStates             = take (length classes) [1,2..] --[[chr y] | y <- [(ord '1')..((length classes) - 1 + (ord '1'))]]
        tAlphabet           = Set.fromList $ getAlphabetFromRules (rules fa)
        tFinals             = Set.toList $ Set.fromList [findClassIndex x classes 1 | x <- finals fa]
        -- Provadi stepeni trid pokud predchozi stav trid a nove vygenerovany stav neni stejny
        -- Vstup: RULES ALPHABET BEFORE
        --      RULES    - Pole pravidel    
        --      ALPHABET - Vstupni abecada
        --      BEFORE   - Predchozi stav trid
        -- Vystup: Usporadana dvojce, kde prvni je pole trid a druhe je mapovani trid
        doMinimalization rules alphabet before = if compare2DArray newClasses before
                then (newClasses, temp)
                else doMinimalization rules alphabet newClasses
            where
                newClasses = fission before temp
                temp = interOverClasses before before alphabet rules
        -- Vytvori pole obsahujici usporadanych dvojic, kde kazda dvojce je tvorena stavem a polem indexu trid, do kterych se dostane z daneho stavu.
        -- Vstup: STATE ALPHA RULES CLASSES CLASS_INDEX
        --      STATE       - Pole stavu
        --      ALPHA       - Vstupni abeceda
        --      RULES       - Pole pravidel
        --      CLASSES     - Pole trid
        -- Vystup: Pole usporadanych dvojic
        interOverClasses :: [Class] -> [Class] -> [Alpha] -> [Rule] -> ClassMapping
        interOverClasses (x:xs) source alpha rules = y1 ++ y2
            where
                y1 = getClassMapping x alpha rules source 
                y2 = interOverClasses xs source alpha rules
        interOverClasses [] _ _ _ = []
        -- Vytvori pole obsahujici usporadanych dvojic, kde kazda dvojce je tvorena stavem a polem indexu trid, do kterych se dostane z daneho stavu.
        -- Vstup: STATE ALPHA RULES CLASSES CLASS_INDEX
        --      STATE       - Pole stavu
        --      ALPHA       - Vstupni abeceda
        --      RULES       - Pole pravidel
        --      CLASSES     - Pole trid
        -- Vystup: Pole usporadanych dvojic
        getClassMapping :: [State] -> [Alpha] -> [Rule] -> [Class] -> ClassMapping
        getClassMapping (x:xs) alpha rules source = [(x, y1)] ++ y2
            where
                y1 = generateMapping x alpha rules source
                y2 = getClassMapping xs alpha rules source
        getClassMapping [] _ _ _ = []
        -- Vytvori pole indexu trid, do kterych se dostaneme z vychoziho stavu za pouziti opdovidajicich pravidel.
        -- Pro vsechny znaky ze vstupni abecedy najdeme index tridy. Pokud neexistuje pravidlo, tak jdeme do tridy s indexem -1.
        -- Vstup: STATE ALPHA RULES CLASSES CLASS_INDEX
        --      STATE       - Vychozi stav
        --      ALPHA       - Pouzity znak z abecedy
        --      RULES       - Pole pravidel
        --      CLASSES     - Pole trid
        -- Vystup: Pole indexu trid
        generateMapping :: State -> [Alpha] -> [Rule] -> [Class] -> [ClassIndex]
        generateMapping state (x:xs) rules source = [y1] ++ y2
            where
                y1 = findClassIndexByAlpha state x rules source 0
                y2 = generateMapping state xs rules source
        generateMapping _ [] _ _ = []
        -- Najde index tridy, do ktere se dostaneme za pouziti konkretniho pravidla
        -- Vstup: STATE ALPHA RULES CLASSES CLASS_INDEX
        --      STATE       - Vychozi stav
        --      ALPHA       - Pouzity znak z abecedy
        --      RULES       - Pole pravidel
        --      CLASSES     - Pole trid
        --      CLASS_INDEX - Soucasny index prohledavane tridy
        -- Vystup: Index tridy (index == -1 pokud trida nebyla nalezena)
        findClassIndexByAlpha :: State -> Alpha -> [Rule] -> [Class] -> ClassIndex -> ClassIndex
        findClassIndexByAlpha state alpha rules (x:xs) index = if endState `elem` x then index else findClassIndexByAlpha state alpha rules xs (index+1)
            where
                endState = existsRule rules state alpha
        findClassIndexByAlpha _ _ _ [] _ = -1
        -- Provede pripadne stepeni trid na zaklade vygenerovaneho mapovani
        -- fission :: [Class] -> ClassMapping -> [Class]
        fission classes table = [x | x <- foldl (++) [] $ map fissionClass classes, length x >= 1]
            where 
                -- Provadi pripadne stepeni dane tridy na zaklade mapovani
                -- Pokud stav ma odlisne mapovani nez prvni stav v dane tride, tak dojde k stepeni, kdy se odlisny stav oddeli od soucanse tridy
                fissionClass :: [State] -> [Class]
                fissionClass [] = []
                fissionClass [x] = [[x]]
                fissionClass (x:xs) = [left, right]
                    where
                        (left, right) = makeFission xs table ([x], [])
                -- Provede rozdeleni tridy na dve nove "podtridy", kdy prvni trida obshuje stavy, ktere maji stejne mapovani jako prvni stav ve tride
                -- a druha trida obsahuje ostatni stavy.
                -- Vstup: FIRST STATES MAPING TEMP
                --      STATES - Mnozina stavu v dane tride
                --      MAPPING - Mapovani stavu
                --      TEMP - Docasny stav vygenerovanych 
                makeFission (x:xs) table (left, right) = if compare1DArray (getMapping (head left) table) (getMapping x table) 
                    then makeFission xs table (left ++ [x], right)
                    else makeFission xs table (left, right ++ [x])
                makeFission [] _ (left, right) = (left, right)

-- Vyhleda odpovidajici mapovani pro zadany stav
getMapping key ((x, mapping):xs) = if key == x then mapping else getMapping key xs

-- V mnozine pravidel vyhleda stav, do ktereho dostanu z vychoziho stavu a zadaneho vstupniho znaku.
-- Vstup: RULES STATE ALPHA
--      RULES - Mnozina pravidel
--      STATE - Vychozi stav
--      ALPHA - Vstupni znak
-- Vystup: Nasledujici stav (pokud pravidlo neexituje tak vraci -1)
existsRule :: [Rule] -> State -> Alpha -> State
existsRule [] _ _ = -1
existsRule [rule] start char = if (from rule) == start && (alpha rule) == char then (to rule) else -1
existsRule (rule:xs) start char = if (from rule) == start && (alpha rule) == char then (to rule) else existsRule xs start char

-- Odstrani z mnoziny stavu stavy obsazene v druhem parametru
removeFromArray :: [State] -> [State] -> [State]
removeFromArray [] _ = []
removeFromArray x [] = x
removeFromArray [x] toRemove = if x `elem` toRemove
    then []
    else [x]
removeFromArray (x:xs) toRemove = if x `elem` toRemove
    then []  ++ removeFromArray xs toRemove
    else [x] ++ removeFromArray xs toRemove

-- Porovna dve 2D pole, jestli obsahuji stejne tridy a ve stejnem poradi
compare2DArray :: [[State]] -> [[State]] -> Bool
compare2DArray [] [] = True
compare2DArray (x:xs) (y:ys) = if compare1DArray x y then compare2DArray xs ys else False
compare2DArray _ _ = False

-- Porovna dve 1D pole, jestli obahuji stejné stavy a ve stejnem poradi
compare1DArray :: [State] -> [State] -> Bool
compare1DArray [] [] = True
compare1DArray (l:ls) (r:rs) = if l == r then compare1DArray ls rs else False
compare1DArray _ _ = False
