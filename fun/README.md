# FLP dka2mka 2017/2018
## Lukas Cerny (*xcerny63*)

---

Nazvy nove vytvorenych stavu jsou index trid, ve kterych se nachazi puvodni stavy. Napriklad: 
```
Stavy: 1,5,7,13,42
Vysledne tridy: [1 => [1,7], 2 => [5], 3 => [13,42]]
Nove stavy: 1,2,3
``` 
Algoritmus minimalizace je prevzany z opory predmetu TIN (Algoritmus 3.5).

Pro overeni funkcnosti, jsem si vytvoril radu testu. V testech overuji praci se vstupem, kombinaci povolenych prepinacu, vstup v nespravnem formatu a ruzne "druhy" minimalizaci.

## Spusteni testu
```
make && cd tests && python3 ./test.py && cd ..
```