#! /usr/bin/env python3

import filecmp
import shlex
import subprocess

PROGRAM_NAME = "../ts"

RED='\033[1;31m'
GREEN='\033[1;32m'
NC='\033[0m'

def getTests():
    return [
        ["./ref-in/test01.in", "./test-out/test01.out", "./ref-out/test01.out", 0, "< {} 2>&1 > {}"],
        ["./ref-in/test02.in", "./test-out/test02.out", "./ref-out/test02.out", 0, "< {} 2>&1 > {}"],
        ["./ref-in/test03.in", "./test-out/test03.out", "./ref-out/test03.out", 0, "< {} 2>&1 > {}"],
        ["./ref-in/test04.in", "./test-out/test04.out", "./ref-out/test04.out", 0, "< {} 2>&1 > {}"],
        ["./ref-in/test05.in", "./test-out/test05.out", "./ref-out/test05.out", 0, "< {} 2>&1 > {}"],
    ]

if __name__ == '__main__':

    for test in getTests():
        command = PROGRAM_NAME + " " + test[4].format(test[0], test[1])
        process = subprocess.run(command, shell=True, stdout=subprocess.PIPE)
        if process.returncode == test[3]:
            if test[3] == 0:
                diff = filecmp.cmp(test[1], test[2])
                if diff:
                    print(GREEN + "OK\t" + test[4].format(test[0], test[1]) + NC)
                else:
                    print(RED + "ERROR\t" + test[4].format(test[0], test[1]) + NC)
            else:
                print(GREEN + "OK\t" + test[4].format(test[0], test[1]) + NC)
        else:
            print(RED + "ERROR\t" + test[4].format(test[0], test[1]) + NC)
