/**
 * Prolog - Turinguv stroj
 * Lukas Cerny - xcerny63
 */
% Precte cely vstup ze STDIN a vrati prectene radky
readLines(Lines) :-
    readLine(Line, LastChar),
    (LastChar == end_of_file, Lines = []; readLines(T), Lines = [Line|T]).
% Precte cely radek ze STDIN a vrati precteny radek a posledni znak
readLine(Line, LastChar) :-
    get_char(Char),
    (isEOForEOL(Char), Line = [], LastChar = Char, !;
    readLine(L, C), Line = [Char|L], LastChar = C).
% Overeni zda vstupni znak je EOF nebo LF
isEOForEOL(Char) :- Char == end_of_file; (char_code(Char, Code), Code == 10).

% Vstupni seznam radku prevede na seznam radku
splitLines([], []).
splitLines([L|Ls], [R|Rs]) :- splitLines(Ls, Rs), splitLine(L, R).
% Rozdeli radek na podseznamy
splitLine([], []).
splitLine([' '|T], S1) :- splitLine(T, S1).
splitLine([32|T], S1) :- splitLine(T, S1).
splitLine([H|T], [H|S1]) :- splitLine(T, S1).

/** Spoji dva seznamy a prvni prevrati */
reverse([], X, X).
reverse([H|T], X, [H|TT]) :- reverse(T, X, TT).

/**  Vypise na STDOUT prvky ze seznamu */
printArrayAsString([]) :- write('\n').
printArrayAsString([H|T]) :- write(H), printArrayAsString(T).

/**
 * Deklarace predikatu rule/4 jako dynamicky
 * Obsahuje STAV ZNAK_POD_HLAVOU NOVY_STAV OPERACE
 */
:- dynamic rule/4.

/** Vypise vsechny stavy pasek TS na STDOUT */
printTapes([]).
printTapes([[L,S,R]|T]) :- printTape(L,S,R), printTapes(T).
% Vypise stav pasky TS na STDOUT
printTape(Left,State,Right) :- reverse(Left, [State|Right], T), printArrayAsString(T).

/**
 * Ze seznamu poli posloupnosti vybere bud tu, ktera vede do koncoveho stavu
 * nebo jakoukoliv jinou vedouci k abnormalnimu zastaveni TS.
 * @param Mnozina vsech posloupnosti
 * @param Vybrana posloupnost
 */
getBestTapes([Best], Best).
getBestTapes([H|T], Best) :- (last(H, [_,State,_]), State == 'F', Best = H; getBestTapes(T,Best)).

/** Z nactenych radku ze STDIN vrati pouze radky obsahujici pravidla */
getRules([_],[]).
getRules([H|T1], [H|T2]) :- getRules(T1, T2).

/** Vlozi fakt (pravidlo) do databaze */
initRules([]).
initRules([[C,A,N,X]|T]) :- rule(C,A,N,X), initRules(T).
initRules([[C,A,N,X]|T]) :- not(rule(C,A,N,X)), assert(rule(C,A,N,X)), initRules(T).

/**
 * Provede simulaci TS
 * @param Stav pasky za hlavou TS
 * @param Stav pasky pod hlavou a pred hlavou
 * @param Soucasny stavu
 * @param Posoupnost vyslednych konfiguraci
 * @param Posloupnot aplikovanych pravidel
 */
simulation(L, R, State,[[L,State,R]],[]) :- State == 'F'.
simulation([],[R|Rs], State, [[[],State,[R|Rs]]|Tapes], [[State, R, NewState, X]|Rules]) :-
  rule(State, R, NewState, X),
  ((X == 'R', simulation([R], Rs, NewState, Tapes, Rules));
   (X == 'L'); % Ukonceni simulace pri vyjeti mimo pasku (vlevo)
   (X \= 'R', X \= 'L', simulation([], [X|Rs], NewState, Tapes, Rules))).
simulation([L|Ls],[R|Rs], State, [[[L|Ls], State, [R|Rs]]|Tapes], [[State, R, NewState, X]|Rules]) :-
  rule(State, R, NewState, X),
  ((X == 'L', append([L,R],Rs, Right), Left = Ls);
   (X == 'R', append([R,L],Ls, Left), Right = Rs);
   (X \= 'R', X \= 'L', Left = [L|Ls], Right = [X|Rs])),
  simulation(Left, Right, NewState, Tapes, Rules).
simulation(L, [], State, [[L,State,[]]],[]). % Ukonceni simulace pri vyjeti mimo pasku (vpravo)
simulation(L, [R|Rs], State, [[L,State,[R|Rs]]],[]) :- not(rule(State, R, _, _)). % Ukonceni simulace pri neexistenci pravidla

main :-
  prompt(_, ''),
  readLines(T),
  splitLines(T, Lines),
  last(Lines, Input),
  getRules(Lines, Rules), initRules(Rules),
  findall(X, simulation([], Input, 'S', X, _), Tapes),
  getBestTapes(Tapes, Result),
  printTapes(Result),
  halt.
