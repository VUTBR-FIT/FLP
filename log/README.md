# FLP TS 2017/2018
## Lukas Cerny (*xcerny63*)

Program precte vstup ze STDIN a z radku reprezentujici pravidla vlozi fakta do databaze (*rule/4*). Pri vkladani jsou ignorovani duplicitni pravidla. 
Pote je spustena simulace TS. Program najde takovou posloupnost konfiguraci, ktera vede do koncoveho stavu.
Pokud takova posloupnost konfiguraci neexistuje, tak vybere takovou posloupnost, ktera vede k abnormalnimu zastaveni.

## Spusteni programu
``` bash
$ make
$ ./ts < input_file
```

## Spusteni testu
``` bash
make test
```
